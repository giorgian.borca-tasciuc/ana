;; CANA: Cleanup ANAlysis
(defpackage "CANA"
  (:use "COMMON-LISP" "CORM" "CLEANUP" "ARITHMETIC-CORM")
  (:export "CLEANUP"))

(in-package cana)
(push (make-instance 'corm) *corms*)
(push (make-instance 'expression-corm) *corms*)
(push (make-instance 'single-addition-corm) *corms*)
(push (make-instance 'addition-corm) *corms*)
(push (make-instance 'single-subtraction-corm) *corms*)
(push (make-instance 'subtraction-corm) *corms*)
(push (make-instance 'single-multiplication-corm) *corms*)
(push (make-instance 'multiplication-corm) *corms*)
(push (make-instance 'zero-multiplication-corm) *corms*)
(push (make-instance 'single-division-corm) *corms*)
(push (make-instance 'division-corm) *corms*)
(push (make-instance 'zero-division-corm) *corms*)
(push (make-instance 'exponent-corm) *corms*)
(push (make-instance 'function-corm
		     :symbol :ln
		     :evaluator #'log
		     :number-parameters 1) *corms*)
(push (make-instance 'function-corm
		     :symbol :sin
		     :evaluator #'sin
		     :number-parameters 1) *corms*)
(push (make-instance 'function-corm
		     :symbol :cos
		     :evaluator #'cos
		     :number-parameters 1) *corms*)
(push (make-instance 'function-corm
		     :symbol :tan
		     :evaluator #'tan
		     :number-parameters 1) *corms*)
(push (make-instance 'function-corm
		     :symbol :sec
		     :evaluator #'(lambda (x) (/ (cos x)))
		     :number-parameters 1) *corms*)
(push (make-instance 'function-corm
		     :symbol :csc
		     :evaluator #'(lambda (x) (/ (sin x)))
		     :number-parameters 1) *corms*)
(push (make-instance 'function-corm
		     :symbol :cot
		     :evaluator #'(lambda (x) (/ (tan x)))
		     :number-parameters 1) *corms*)

;; TODO: Add arc* functions
