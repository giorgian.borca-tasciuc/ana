(defun listcat (&rest seqs)
  (apply #'concatenate 'list seqs))

(defclass jorm () ())
(defmethod conforms ((f jorm) op e1 e2)
  t)

(defmethod join :before ((f jorm) op e1 e2)
  (assert (conforms f op e1 e2)))

(defmethod join ((f jorm) op e1 e2)
  `(,op ,e1 ,e2))

(defparameter *jorms* nil)
(defun join-expression (op e1 e2)
  (loop for jorm in *jorms* when (conforms jorm op e1 e2)
       return (join jorm op e1 e2)))
       
(defclass @{@@}-jorm (jorm)
  ((symbol
    :initarg :symbol
    :initform (error "Function symbol unspecified.")
    :reader @{@@}-jorm-symbol)))

(defmethod conforms ((f @{@@}-jorm) op e1 e2)
  (let ((sym (@{@@}-jorm-symbol f)))
    (and (eq op sym)
	 (listp e1)
	 (listp e2)
	 (eq (first e1) sym)
	 (eq (first e2) sym))))

(defmethod join ((f @{@@}-jorm) op e1 e2)
  (let ((sym (@{@@}-jorm-symbol f)))
    `(,sym ,@(rest e1) ,@(rest e2))))


(defclass @{@>@}-jorm (jorm)
  ((symbol
    :initarg :symbol
    :initform (error "Function symbol unspecified.")
    :reader @{@>@}-jorm-symbol)))

(defmethod conforms ((f @{@>@}-jorm) op e1 e2)
  (let ((sym (@{@>@}-jorm-symbol f)))
    (and (eq op sym)
	 (flet ((test (e1 e2)
		  (and (listp e1)
		       (eq (first e1) sym)
		       (or (not (listp e2))
			   (not (eq (first e2) sym))))))
	   (or (test e1 e2) (test e2 e1))))))

(defmethod join ((f @{@>@}-jorm) op e1 e2)
  (let* ((sym (@{@>@}-jorm-symbol f))
	 (p (if (and (listp e1) (eq (first e1) sym)) e1 e2))
	 (o (if (and (listp e1) (eq (first e1) sym)) e2 e1)))
    `(,sym ,@(rest p) ,o)))

(defclass *{+>+}-jorm (jorm) ())
(defmethod conforms ((f *{+>+}-jorm) op e1 e2)
  (and (eq op :*)
      (flet ((test (e1 e2)
	       (and (listp e1)
		    (eq (first e1) :+)
		    (or (not (listp e2))
			(not (eq (first e2) :+))))))
	(or (test e1 e2) (test e2 e1)))))

(defmethod join ((f *{+>+}-jorm) op e1 e2)
  (let ((p (if (and (listp e1) (eq (first e1) :+)) e1 e2))
	(o (if (and (listp e1) (eq (first e1 ):+)) e2 e1)))
    `(:+ ,@(loop for e in (rest p)
	      collect (join-expression :* e o)))))

(defclass *{++}-jorm (jorm) ())
(defmethod conforms ((f *{++}-jorm) op e1 e2)
  (and (eq op :*)
       (listp e1)
       (listp e2)
       (eq (first e1) :+)
       (eq (first e2) :+)))

(defmethod join ((f *{++}-jorm) op e1 e2)
  `(:+ ,@(apply #'listcat (loop for e in (rest e1)
			     collect (rest (join-expression :* e e2))))))

(defclass ^_+-jorm (jorm) ())
(defmethod conforms ((f ^_+-jorm) op e1 e2)
  (and (eq op :^)
       (listp e2)
       (eq (first e2) :+)))

(defmethod join ((f ^_+-jorm) op e1 e2)
  (flet ((e-join (e2) (join-expression :^ e1 e2))
	 (m-join (e1 e2) (join-expression :* e1 e2)))
    (let* ((params (rest e2))
	   (m (mapcar #'e-join params)))
      (reduce #'m-join m))))

(defclass ^_*-jorm (jorm) ())
(defmethod conforms ((f ^_*-jorm) op e1 e2)
  (and (eq op :^)
       (listp e2)
       (eq (first e2) :*)))

(defmethod join ((f ^_*-jorm) op e1 e2)
  (flet ((e-join (e1 e2) (join-expression :^ e1 e2)))
    (reduce #'e-join (rest e2) :initial-value e1)))

;; TODO: move into seperate, configurable package where
;; user has the ability to add numeric functions
;; Make something similar where users can also define constants

;; TODO: Add arc* functions
(defun is-numeric-function (f)
  (member f '(:+ :- :* :/ :^ :ln :sin :cos :tan :sec :csc :cot)))

(defun is-numeric (e)
  (or (numberp e)
      (and (listp e)
	   (is-numeric-function (first e))
	   (every #'is-numeric (rest e)))))

(defun evaluate-numeric (f)
  (if (numberp f)
      f
      (let ((eparams (mapcar #'evaluate-numeric (rest f))))
	(case (first f)
	  (:+ (apply #'+ eparams))
	  (:* (apply #'* eparams))
	  (:/ (apply #'/ eparams))
	  (:^ (apply #'expt eparams))
	  (:ln (apply #'log eparams))
	  (:sin (apply #'sin eparams))
	  (:cos (apply #'cos eparams))
	  (:tan (apply #'tan eparams))
	  (:sec (apply #'(lambda (e) (/ (cos e))) eparams))
	  (:csc (apply #'(lambda (e) (/ (sin e))) eparams))
	  (:cot (apply #'(lambda (e) (/ (tan e))) eparams))))))
     
(defclass ^_N-jorm (jorm) ())
(defmethod conforms ((f ^_N-jorm) op e1 e2)
  (and (eq op :^)
       (is-numeric e2)))

(defmethod join ((f ^_N-jorm) op e1 e2)
  (let ((n (evaluate-numeric e2)))
    (flet ((m-join (e1 e2) (join-expression :* e1 e2)))
      (cond ((= 0 n) 1)
	    ((< n 0)
	     ;; TODO: deal with partial fraction differntiation
	     (reduce #'m-join (loop for i from 1 to n
				 collect `(:^ ,e1 -1)))) 
	    ((> n 0)
	     (reduce #'m-join (loop for i from 1 to n collect e1)))))))
	   
(push (make-instance 'jorm) *jorms*)
(push (make-instance '@{@@}-jorm :symbol :+) *jorms*)
(push (make-instance '@{@>@}-jorm :symbol :+) *jorms*)
(push (make-instance '@{@@}-jorm :symbol :*) *jorms*)
(push (make-instance '@{@>@}-jorm :symbol :*) *jorms*)
(push (make-instance '*{+>+}-jorm) *jorms*)
(push (make-instance '*{++}-jorm ) *jorms*)
(push (make-instance '^_+-jorm) *jorms*)
(push (make-instance '^_*-jorm) *jorms*)
(push (make-instance '^_N-jorm) *jorms*)
