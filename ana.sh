#!/bin/sh

clear
exec sbcl --load dorm.l --load derivative.l --load arithmetic-dorm.l --load trigonometric-dorm.l --load dana.l --load corm.l --load cleanup.l --load arithmetic-corm.l --load cana.l
