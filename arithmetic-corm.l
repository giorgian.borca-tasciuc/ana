(defpackage "ARITHMETIC-CORM"
  (:use "COMMON-LISP" "CORM" "CLEANUP")
  (:export "EXPRESSION-CORM"
	   "SINGLE-ADDITION-CORM" "ADDITION-CORM"
	   "SINGLE-SUBTRACTION-CORM" "SUBTRACTION-CORM"
	   "SINGLE-MULTIPLICATION-CORM" "MULTIPLICATION-CORM"
	   "ZERO-MULTIPLICATION-CORM"
	   "SINGLE-DIVISION-CORM" "DIVISION-CORM"
	   "ZERO-DIVISION-CORM"
	   "EXPONENT-CORM"
	   "FUNCTION-CORM"
	   "MATCHES" "CLEAN"))

(in-package arithmetic-corm)

(defun num= (a b)
  (and (numberp a)
       (numberp b)
       (= a b)))

(defun num-or-f (f)
  (lambda (e)
    (or (numberp e)
	(matches f e))))

(defun listcat (&rest seqs)
  (apply #'concatenate 'list seqs))

(defun bound-matches (f)
  (lambda (e) (matches f e)))

(defclass expression-corm (corm) ())
(defmethod matches ((f expression-corm) e)
  (listp e))

(defmethod clean ((f expression-corm) e)
  (let ((cleaned (mapcar #'cleanup (rest e))))
    `(,(first e) ,@cleaned)))

(defclass single-addition-corm (corm) ())
(defmethod matches ((f single-addition-corm) e)
  (and (listp e)
       (eq (first e) :+)
       (= (length e) 2)))

(defmethod clean ((f single-addition-corm) e)
  (cleanup (second e)))
  
(defclass addition-corm (corm) ())
(defmethod matches ((f addition-corm) e)
  (and (listp e)
       (eq (first e) :+)
       (>= (length e) 3)))

(defmethod clean ((f addition-corm) e)
  (let* ((cleaned (mapcar #'cleanup (rest e)))
	 (numbers (remove-if-not #'numberp cleaned))
	 (other (remove-if (num-or-f f) cleaned))
	 (sub (remove-if-not (bound-matches f) cleaned))
	 (nested (apply #'listcat (mapcar #'rest sub))))
    (cleanup-without f (remove 0 `(:+ ,(apply #'+ numbers)
				      ,@nested
				      ,@other)
			       :test #'num=))))

(defclass single-subtraction-corm (corm) ())
(defmethod matches ((f single-subtraction-corm) e)
  (and (listp e)
       (eq (first e) :-)
       (= (length e) 2)))

(defmethod clean ((f single-subtraction-corm) e)
  (let ((cleaned (cleanup (second e))))
    (if (numberp cleaned)
	(- cleaned)
      `(:- ,cleaned))))

(defclass subtraction-corm (corm) ())
(defmethod matches ((f subtraction-corm) e)
  (and (listp e)
       (eq (first e) :-)
       (>= (length e) 3)))

(defmethod clean ((f subtraction-corm) e)
  (let* ((cleaned (mapcar #'cleanup (rest e)))
	 (numbers (remove-if-not #'numberp (rest cleaned)))
	 (minuend (first cleaned))
	 (other (remove-if #'numberp (rest cleaned)))
	 (sum (apply #'+ numbers)))
    (cond
     ((and (numberp minuend)
	   (null other)) (- minuend sum))
     ((= sum 0) (cleanup-without f `(:- ,@other)))
     (t (cleanup-without f `(:- ,@other ,sum))))))

(defclass single-multiplication-corm (corm) ())
(defmethod matches ((f single-multiplication-corm) e)
  (and (listp e)
       (eq (first e) :*)
       (= (length e) 2)))

(defmethod clean ((f single-multiplication-corm) e)
  (cleanup (second e)))

(defclass multiplication-corm (corm) ())
(defmethod matches ((f multiplication-corm) e)
  (and (listp e)
       (eq (first e) :*)
       (>= (length e) 3)))

(defmethod clean ((f multiplication-corm) e)
  (let* ((cleaned (mapcar #'cleanup (rest e)))
	 (numbers (remove-if-not #'numberp cleaned))
	 (other (remove-if (num-or-f f) cleaned))
	 (sub (remove-if-not (bound-matches f) cleaned))
	 (nested (apply #'listcat (mapcar #'rest sub))))
    (cleanup-without f (remove 1 `(:* ,(apply #'* numbers)
				      ,@nested
				      ,@other)
			       :test #'num=))))

(defclass zero-multiplication-corm (corm) ())
(defmethod matches ((f zero-multiplication-corm) e)
  (and (listp e)
       (eq (first e) :*)
       (member 0 e :test #'num=)))

(defmethod clean ((f zero-multiplication-corm) e)
  0)
					
(defclass single-division-corm (corm) ())
(defmethod matches ((f single-division-corm) e)
  (and (listp e)
       (eq (first e) :/)
       (= (length e) 2)))

(defmethod clean ((f single-division-corm) e)
  (cleanup (second e)))

(defclass division-corm (corm) ())
(defmethod matches ((f division-corm) e)
  (and (listp e)
       (eq (first e) :/)
       (>= (length e) 3)))

(defmethod clean ((f division-corm) e)
  (let* ((cleaned (mapcar #'cleanup (rest e)))
	 (numbers (remove-if-not #'numberp (rest cleaned)))
	 (numerator (first cleaned))
	 (other (remove-if #'numberp (rest cleaned)))
	 (product (apply #'* numbers)))
    (cond
     ((and (numberp numerator)
	   (null other)) (/ numerator product))
     ((= product 1) (cleanup-without f `(:/ ,@other)))
     (t (cleanup-without f `(:/ ,@other ,product))))))

(defclass zero-division-corm (corm) ())
(defmethod matches ((f zero-division-corm) e)
  (and (listp e)
       (eq (first e) :/)
       (num= (second e) 0)))

(defmethod clean ((f zero-division-corm) e)
  0)

(defclass exponent-corm (corm) ())
(defmethod matches ((f exponent-corm) e)
  (and (listp e)
       (eq (first e) :^)
       (= (length e) 3)))

(defmethod clean ((f exponent-corm) e)
  (let* ((base (cleanup (second e)))
	 (power (cleanup (third e))))
    (cond
     ((and (numberp base) (numberp power)) (expt base power))
     ((num= power 0) 1)
     ((num= power 1) base)
     (t `(:^ ,base ,power)))))

(defclass function-corm (corm)
  ((symbol
    :initarg :symbol
    :initform (error "Function symbol unspecified.")
    :reader function-corm-symbol)
   (evaluator
    :initarg :evaluator
    :initform nil
    :reader function-corm-evaluator)
   (number-parameters
    :initarg :number-parameters
    :initform (error "Number of parameters unspecified.")
    :reader function-corm-number-parameters)))
(defmethod matches ((f function-corm) e)
  (and (listp e)
       (eq (first e) (function-corm-symbol f))
       (= (length (rest e)) (function-corm-number-parameters f))))

(defmethod clean ((f function-corm) e)
  (let* ((cleaned (mapcar #'cleanup (rest e)))
	 (numbers (remove-if-not #'numberp cleaned))
	 (other (remove-if #'numberp cleaned)))
    (if (and (null other) (function-corm-evaluator f))
	(apply (function-corm-evaluator f) numbers)
	`(,(function-corm-symbol f) ,@cleaned))))
