;; norm: Normal fORM
;; Normal form hierarchy:
;; :+
;; :*
;; :^
;; symbols or function calls such as ln,sin,etc

(defclass norm () ())
(defmethod matches ((f norm) e)
  nil)

(defmethod normalize ((f norm) e)
    e)

(defmethod normalize :before ((f norm) e)
  (assert (matches f e)))


(defclass jorm () ())
(defmethod conforms ((f jorm) op e1 e2)
  t)

(defmethod join :before ((f jorm) op e1 e2)
  (assert (conforms f op e1 e2)))

(defmethod join ((f jorm) op e1 e2)
  `(,op ,e1 ,e2))

(defclass +{++}-jorm (jorm) ())
(defmethod conforms ((f +{++}-jorm) op e1 e2)
  (and (eq op :+)
       (listp e1)
       (listp e2)
       (eq (first e1) :+)
       (eq (first e2) :+)))

(defclass +{+>+}-jorm (jorm) ())
(defmethod conforms ((f +{+>+}-jorm) op e1 e2)
  (and (eq op :+)
       (flet ((test (e1 e2)
		(and (listp e1)
		     (eq (first e1) :+)
		     (or (not (listp e2))
			 (not (eq (first e2) :+))))))
	 (or (test e1 e2) (test e2 e1)))))

;(defmethod join (jorm) ((f +{+>+}-jorm op e1 e2))
     
  


(defmethod join ((f +{++}-jorm) op e1 e2)
  `(:+ ,@(rest e1) ,@(rest e2)))

(defclass *{**}-jorm (jorm) ())
(defmethod conforms ((f *{**}-jorm) op e1 e2)
  (and (eq op :*)
       (listp e1)
       (listp e2)
       (eq (first e1) :*)
       (eq (first e2) :*)))

(defmethod join ((f *{**}-jorm) op e1 e2)
  `(:* ,@(rest e1) ,@(rest e2)))

(defclass *{+>+}-jorm (jorm) ())
(defmethod conforms ((f *{+>+}-jorm) op e1 e2)
  (and (eq op :*)
      (flet ((test (e1 e2)
	       (and (listp e1)
		    (eq (first e1) :+)
		    (or (not (listp e2))
			(not (eq (first e2) :+))))))
	(or (test e1 e2) (test e2 e1)))))

(defmethod join (jorm) ((f *{+>+}-jorm) op e1 e2)
  (let ((p (if (and (listp e1) (eq (first e1) :+)) e1 e2))
	(o (if (and (listp e1) (e1 (first e1 :+)) e2 e2))))
    ; remember to combine results using (combine)
      `(:+ ,@(apply #'listcat (loop 
      

		 
      

(defclass *{++}-jorm (jorm) ())
(defmethod conforms ((f *{++}-jorm) op e1 e2)
  (and (eq op :*)
       (listp e1)
       (listp e2)
       (eq (first e1) :+)
       (eq (first e2) :*)))

       
    



(defparameter *norms* nil)
(defun normalize-expression (e v)
  (loop for norm in *norms* when (matches norm e v)
     return (normalize norm e v)))

(defun bound-matches (f)
  (lambda (e) (matches f e)))

(defun listcat (&rest seqs)
  (apply #'concatenate 'list seqs))

(defclass addition-norm (norm))
(defmethod matches ((f addition-norm) e)
  (and (listp e)
       (eq (first e) :+)))

(defmethod normalize ((f addition-norm) e)
  (let* ((normalized (mapcar #'normalize-expression (rest e)))
	 (sub (remove-if-not #'(bound-matches f) (rest e)))
	 (other (remove-if #'(bound-matches f) (rest e)))
	 (nested (apply #'listcat (mapcar #'rest sub))))
    `(:+ ,@other ,@nested)))

(defclass multiplication-norm (norm))
(defmethod matches ((f multiplication-norm) e)
  (and (listp)
       (eq (first e) :*)))

(defun both (f1 f2)
  (lambda (e) (and (f1 e) (f2 e))))

(defmethod normalize ((f multiplication-norm) e)
  (let* ((normalized (mapcar #'normalize-expression (rest e)))
	 (sub (remove-if-not #'(bound-matches f) (rest e)))
	 (other (remove-if #'(bound-matches f) (rest e)))
	 (nested (apply #'listcat (mapcar #'rest sub))))
    `(:+ ,@other ,@nested)))

