(defpackage "TRIGONOMETRIC-DORM"
  (:use "COMMON-LISP" "DORM" "DERIVATIVE")
  (:export "SIN-DORM" "COS-DORM" "TAN-DORM" "SEC-DORM" "CSC-DORM" "COT-DORM"
	   "ARCSIN-DORM" "ARCCOS-DORM" "ARCTAN-DORM" "ARCSEC-DORM" "ARCCSC-DORM" "ARCCOT-DORM"
	   "MATCHES" "DIFFERENTIATE"))

(in-package trigonometric-dorm)
(defclass sin-dorm (dorm) ())
(defmethod matches ((f sin-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :sin)))

(defmethod differentiate ((f sin-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* (:cos ,u) ,du)))

(defclass cos-dorm (dorm) ())
(defmethod matches ((f cos-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :cos)))

(defmethod differentiate ((f cos-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* -1 (:sin ,u) ,du)))

(defclass tan-dorm (dorm) ())
(defmethod matches ((f tan-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :tan)))

(defmethod differentiate ((f tan-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* (:^ 2 (:sec ,u)) ,du)))

(defclass sec-dorm (dorm) ())
(defmethod matches ((f sec-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :sec)))

(defmethod differentiate ((f sec-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* (:sec ,u) (:tan ,u) ,du)))

(defclass csc-dorm (dorm) ())
(defmethod matches ((f csc-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :csc)))

(defmethod differentiate ((f csc-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* -1 (:csc ,u) (:cot ,u) ,du)))

(defclass cot-dorm (dorm) ())
(defmethod matches ((f cot-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :cot)))

(defmethod differentiate ((f cot-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* -1 (:^ 2 (:csc ,u)) ,du)))

(defclass arcsin-dorm (dorm) ())
(defmethod matches ((f arcsin-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :arcsin)))

(defmethod differentiate ((f arcsin-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* (:^ -.5 (:- 1 (:^ ,u 2))) ,du)))

(defclass arccos-dorm (dorm) ())
(defmethod matches ((f arccos-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :arccos)))

(defmethod differentiate ((f arccos-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* -1 (:^ -.5 (:- 1 (:^ ,u 2))) ,du)))


(defclass arctan-dorm (dorm) ())
(defmethod matches ((f arctan-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :arctan)))

(defmethod differentiate ((f arctan-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* (:^ (:+ (:^ ,u 2) 1) -1) ,du)))


(defclass arcsec-dorm (dorm) ())
(defmethod matches ((f arcsec-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :arcsec)))

(defmethod differentiate ((f arcsec-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* (:^ (:abs ,u) -1) (:^ -.5 (:- (:^ ,u 2) 1)) ,du)))

(defclass arccsc-dorm (dorm) ())
(defmethod matches ((f arccsc-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :arccsc)))

(defmethod differentiate ((f arccsc-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* -1 (:^ (:abs ,u) -1) (:^ -.5 (:- (:^ ,u 2) 1)) ,du)))

(defclass arccot-dorm (dorm) ())
(defmethod matches ((f arccot-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :arccot)))

(defmethod differentiate ((f arccot-dorm) e (v symbol))
  (let* ((u (second e))
	 (du (derivative-of u v)))
    `(:* -1 (:^ (:+ (:^ ,u 2) 1) -1) ,du)))
