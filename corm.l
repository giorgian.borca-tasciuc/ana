;; corm: Clean fORM
(defpackage "CORM"
  (:use "COMMON-LISP")
  (:export "CORM" "MATCHES" "CLEAN"))

(in-package corm)

(defclass corm () ())
(defmethod matches ((f corm) e)
  t)

(defmethod clean :before ((f corm) e)
  (format t "~A cleaning ~A~%" f e)
  (assert (matches f e)))

(defmethod clean ((f corm) e)
  e)

