(defpackage "CLEANUP"
  (:use "COMMON-LISP" "CORM")
  (:export "CLEANUP" "CLEANUP-WITHOUT" "*CORMS*"))

(in-package cleanup)

(defparameter *corms* nil)
(defun cleanup-helper (corms e)
  (loop for corm in corms when (matches corm e) return
	(clean corm e)))

(defmethod cleanup (e)
  (cleanup-helper *corms* e))

(defmethod cleanup-without ((f corm) e)
  (cleanup-helper (remove f *corms*) e))
  
