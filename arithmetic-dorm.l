(defpackage "ARITHMETIC-DORM"
  (:use "COMMON-LISP" "DORM" "DERIVATIVE")
  (:export "CONSTANT-DORM" "VARIABLE-DORM" "ADDITION-DORM" "SUBTRACTION-DORM" "SINGLE-MULTIPLICATION-DORM"
	   "MULTIPLICATION-DORM" "SINGLE-DIVISION-DORM" "DIVISION-DORM" "CONSTANT-EXPONENT-DORM"
	   "CONSTANT-BASE-EXPONENT-DORM" "LN-DORM" "MATCHES" "DERIVE"))


(in-package arithmetic-dorm)
(defun is-constant (e v)
  (or (numberp e)
      (and (symbolp e) (not (string= e v)))
      (and (listp e) (every #'(lambda (elem) (is-constant elem v))
			    (rest e)))))

(defclass constant-dorm (dorm) ())
(defmethod matches ((f constant-dorm) e (v symbol))
  (is-constant e v))

(defmethod differentiate ((f constant-dorm) e (v symbol))
  0)

(defclass variable-dorm (dorm) ())
(defmethod matches ((f variable-dorm) e (v symbol))
  (eq e v))

(defmethod differentiate ((f variable-dorm) e (v symbol))
  1)

(defclass addition-dorm (dorm) ())
(defmethod matches ((f addition-dorm) e (v symbol))
  (and (listp e)
       (eq (first e) :+)))

(defmethod differentiate ((f addition-dorm) e (v symbol))
  (cons :+ (loop for operand in (rest e) collect (derivative-of operand v))))

(defclass subtraction-dorm (dorm) ())
(defmethod matches ((f subtraction-dorm) e (v symbol))
  (and (listp e)
       (eq (first e) :-)))

(defmethod differentiate ((f subtraction-dorm) e (v symbol))
  (let* ((operands (rest e))
	 (rewritten `(:+ ,(first operands) (:* -1 (:+ ,@(rest operands))))))
    (derivative-of rewritten v)))

(defun init (l)
  (subseq l 0 (1- (length l))))

(defclass single-multiplication-dorm (dorm) ())
(defmethod matches ((f single-multiplication-dorm) e (v symbol))
  (and (listp e)
       (eq (first e) :*)
       (= (length e) 2)))

(defmethod differentiate ((f single-multiplication-dorm) e (v symbol))
  (derivative-of (second e) v))

(defclass multiplication-dorm (dorm) ())
(defmethod matches ((f multiplication-dorm) e (v symbol))
  (and (listp e)
       (eq (first e) :*)
       (>= (length e) 3)))

(defmethod differentiate ((f multiplication-dorm) e (v symbol))
  (let* ((operands (rest e))
	 (f (cons :* (init operands)))
	 (g (first (last operands)))
	 (f* (derivative-of f v))
	 (g* (derivative-of g v)))
    `(:+ (:* ,f ,g*) (:* ,g ,f*))))

(defclass single-division-dorm (dorm) ())
(defmethod matches ((f single-division-dorm) e (v symbol))
  (and (listp e)
       (eq (first e) :/)
       (= (length e) 2)))

(defmethod differentiate ((f single-division-dorm) e (v symbol))
  (derivative-of (second e) v))

(defclass division-dorm (dorm) ())
(defmethod matches ((f division-dorm) e (v symbol))
  (and (listp e)
       (eq (first e) :/)
       (>= (length e) 3)))

(defmethod differentiate ((f division-dorm) e (v symbol))
  (let* ((operands (rest e))
	 (f (cons :/ (init operands)))
	 (g (first (last operands)))
	 (f* (derivative-of f v))
	 (g* (derivative-of g v)))
    `(:/ (:- (:* ,g ,f*) (:* ,f ,g*)) (:^ ,g 2))))

(defclass constant-exponent-dorm (dorm) ())
(defmethod matches ((f constant-exponent-dorm) e (v symbol))
  (and (listp e)
       (= 3 (length e))
       (eq (first e) :^)
       (is-constant (third e) v)))

(defmethod differentiate ((f constant-exponent-dorm) e (v symbol))
  (let* ((base (second e))
	 (exponent (third e))
	 (d (derivative-of base v)))
    `(:* ,exponent (:^ ,base (:- ,exponent 1)) ,d)))

(defclass constant-base-exponent-dorm (dorm) ())
(defmethod matches ((f constant-base-exponent-dorm) e (v symbol))
  (and (listp e)
       (= 3 (length e))
       (eq (first e) :^)
       (is-constant (second e) v)
       (not (is-constant (third e) v))))

(defmethod differentiate ((f constant-base-exponent-dorm) e (v symbol))
  (let* ((c (second e))
	 (u (third e))
	 (pow `(:* (:ln ,c) ,u)))
    `(:* ,(derivative-of pow v) (:^ :e ,pow))))

(defclass ln-dorm (dorm) ())
(defmethod matches ((f ln-dorm) e (v symbol))
  (and (listp e)
       (= (length e) 2)
       (eq (first e) :ln)))

(defmethod differentiate ((f ln-dorm) e (v symbol))
  (let ((u (second e)))
    `(:/ ,(derivative-of u v) ,u)))
