;;; Note: A dorm is a Derivative fORM
(defpackage "DORM"
  (:use "COMMON-LISP")
  (:export "DORM" "MATCHES" "DIFFERENTIATE"))

(in-package dorm)

(defclass dorm () ())
(defmethod matches ((f dorm) e (v symbol))
    t)

(defmethod differentiate :before ((f dorm) e (v symbol))
  (format t "~A deriving ~A~%" f e)
  (assert (matches f e v)))

(defmethod differentiate ((f dorm) e (v symbol))
  `(:derivative-of ,e ,v))
