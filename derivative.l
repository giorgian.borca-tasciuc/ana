(defpackage "DERIVATIVE"
  (:use "COMMON-LISP" "DORM")
  (:export "*DORMS*" "DERIVATIVE-OF"))

(in-package derivative)

(defparameter *dorms* nil)
(defun derivative-of (e v)
  (loop for dorm in *dorms* when (matches dorm e v)
	return (differentiate dorm e v)))
